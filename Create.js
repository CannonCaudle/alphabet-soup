//Cannon Caudle
//10-15-2019


function create(inputData) {
    var grid = [], size = [], key = [];
    var gridOffSet = 3;
    //valid chars
    var regex=/^[a-zA-Z]+$/;

    var splitvalues = inputData.match(/[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g);
    size = [splitvalues[0], splitvalues[2]];

    if(size[0] >= (Number.MAX_SAFE_INTEGER/2) || size[1] >= (Number.MAX_SAFE_INTEGER/2)){
        printTooLargeGrid(); 
    }
    if(isNaN(size[0]) != false || isNaN(size[0]) != false){
        printNotANumber();
    }

    var gridSize = ((size[0] * size[1]) + gridOffSet);
    if(gridSize >= Number.MAX_SAFE_INTEGER){
        printTooLargeGrid();
    }
    // create an array of key words
    function createKey(keyData, size) {
        var lines = keyData.split('\n');
        var frontbuffer = Number(size[0]) +1;
        var i;

        for (i = lines.length-1; i >= frontbuffer ; i--) {
            lines[i] = lines[i].replace(/\s+/g, '').toLowerCase();
            if(!lines[i].match(regex)){
                printNotAString();
            }
            if(lines[i].length >= Number.MAX_SAFE_INTEGER || lines[i].length > size[0] && lines[i].length > size[1]){
                printTooLongAnswer();
            }

            if(lines[i].length < 1){
                printTooShort();
            }
            key.push(lines[i]);
            
        };
        return key;
    };

    // create the grid
    function createGrid(inputData, gridOffSet, gridSize) {
        //format
        inputData = inputData.replace(/\s+/g, '').toLowerCase(); 
        var gridChars = inputData.slice(gridOffSet, gridSize);
        var count = 0;
        var i,j;
        for (i = 0; i < size[0]; i++) {
            var row = [];
            for (j = 0; j < size[1]; j++) {
                if(!gridChars[count].match(regex)){
                    printNotAString();
                }
                row.push(gridChars[count]);
                count = count + 1;
            }
            grid.push(row);

        };

        if(size[0] != (grid.length) || size[1] != grid[0].length ){       
            printSizeIsOff();
        }
        return grid;
    };



    return {
        first: createGrid(inputData, gridOffSet, gridSize),
        second: createKey(inputData, size),
        third: size
    };
};