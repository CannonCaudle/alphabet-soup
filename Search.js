//Cannon Caudle
//10-15-2019

function search(inputData) {
    //break out data
    var size = inputData.third;
    var R = size[0], C = size[1];
    var key = inputData.second;
    var grid = inputData.first;

    searchGrid(grid, key);

    function searchGrid(grid, key) {
        var row, col, i;
        for (i = 0; i < key.length; i++) {
            for (row = 0; row < R; row++) {
                for (col = 0; col < C; col++) {
                    find(grid, row, col, key[i], R, C);
                };
            };
        };
        printIfEmpty();
    };

    //checkable coordinates
    function find(grid, row, col, word, R, C) {
        var x = [-1, -1, -1, 0, 0, 1, 1, 1];
        var y = [-1, 0, 1, -1, 1, -1, 0, 1];

        if (grid[row][col] != word[0])
            return false;

        else {
            var len = word.length;

            var dir, rowEnd, colEnd;
            for (var dir = 0; dir < 8; dir++) {

                //check if possible fit and find final location.
                rowEnd = row + ((len - 1) * x[dir]);
                colEnd = col + ((len - 1) * y[dir]);

                if (rowEnd >= R || colEnd >= C || rowEnd < 0 || colEnd < 0) {

                    continue;
                }

                
                var rowD = row + x[dir];
                var colD = col + y[dir];
                var check;
                for (check = 1; check < len; check++) {
                    // if not a match
                    if (grid[rowD][colD] != word[check]) {
                        break;
                    }

                    rowD += x[dir], colD += y[dir];
                }
                if (check == len) {

                    printing(word, row, col, rowEnd, colEnd);
                    return true;
                }
            }
        }
        return false;

    };
};